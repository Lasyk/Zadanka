//	Zadanie
//	Proszę napisać funkcję, która otrzymawszy skompresowany string zwróci rozpakowany tekst.
//
//	Dane wejściowe są spakowane przy pomocy wyrażeń N[PackedString]. Przy rozpakowaniu, znajdujący się w nawiasach kwadratowych PackedString, jest powtarzany N razy. Proszę zauważyć, że N to liczba całkowita większa od zera.
//
//	Można założyć, że dane wejściowe są zawsze poprawne: bez białych znaków, nawiasy poprawnie sparowane, itd.
//
//	Ponadto można założyć, że tekst przed kompresją nie zawierał żadnych cyfr, tak więc jedyne cyfry to te składające się na liczbę N opisującą liczbę powtórzeń. Nie będzie danych wejściowych, takich jak 2b czy 3[1]
//
//	Przykłady:
//	Wywołanie z "2[a]3[bc]", zwraca "aabcbcbc"
//	Wywołanie z "3[d2[e]]", zwraca "deedeedee"
//	Wywołanie z "fg2[eset]3[hi]", zwraca " fgesetesethihihi"
//

#include <vector>
#include <iostream>
using namespace std;

template<typename Iter>
pair<Iter, string> parse(Iter b, Iter e)
{
	string result;
	auto it = b;

	while(it != e && *it != ']')
	{
		// pattern N[] found
		if(isdigit(*it))
		{
			unsigned num = *it - '0';
			auto tmp = parse(it+2, e);
			it = tmp.first;
			for(unsigned i=0; i<num; ++i)
				result += tmp.second;
		}
		else
		{
			result += *it;
		}

		++it;
	}

	return make_pair(it, result);
}

int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */
	string str1 = "2[a]3[bc]";
	string str2 = "3[d2[e]]";
	string str3 = "fg2[eset]3[hi]";
	vector<char> str4(str3.begin(), str3.end());

	cout << parse(str1.begin(), str1.end()).second << endl
		 << parse(str2.begin(), str2.end()).second << endl
	     << parse(str3.begin(), str3.end()).second << endl
		 << parse(str4.begin(), str4.end()).second << endl;

    return 0;
}
