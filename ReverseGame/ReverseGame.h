#pragma once

#include<iostream>
#include<vector>
#include<algorithm>
#include<iterator>

class ReverseGame {
public:
   void run() {
      unsigned ballNumber, ball;
      std::cin >> ballNumber >> ball;

      if (ball >= ballNumber) {
         std::cout << "There is no ball with number: "<< ball << std::endl;
         return;
      }

//      std::cout << "Result is: " << calculate(ballNumber, ball) << std::endl;
      std::cout << "Result is: " << calculate2(ballNumber, ball) << std::endl;
   }

protected:
   unsigned calculate(unsigned ballNumber, unsigned ball) {
      std::vector<unsigned> balls(ballNumber);

      /// generate balls numbers 
      int n{ 0 };
      std::generate(balls.begin(), balls.end(), [&n] { return n++; });

      /// make reverse for every ball
      for (auto it = balls.begin(); it != balls.end(); ++it)
         std::reverse(it, balls.end());

      return balls[ball - 1];
   }

   /// wykorzystuje fakt że ciąg odpowiedzi to 
   /// reversed[0], balls[0], reversed[1], balls[1], reversed[2], balls[2], ...
   unsigned calculate2(unsigned ballNumber, unsigned ball) {
      ball = ball -1;
      std::vector<unsigned> balls(ballNumber), reversed(ballNumber);
      std::generate(balls.begin(), balls.end(), [n = 0]() mutable { return n++; });
      std::reverse_copy(balls.begin(), balls.end(), reversed.begin());

      if (ball % 2)  /// ball is even
         return balls[ball/2];

      /// ball is odd take from reversed container
      return reversed[ball/2];
   }

private:
   unsigned testCases;
};