#include <iomanip>
#include <iostream>

template < size_t N >
struct Matrix
{
   constexpr Matrix() = default;
   constexpr auto size() const noexcept { return N; }

   constexpr auto begin() const { return std::cbegin(matrix); }
   constexpr auto end() const { return std::cend(matrix); }

   int matrix[N];
};

template < typename T, size_t N >
constexpr Matrix< N * N > makeMatrix(T (&tab)[N][N]) noexcept
{
   Matrix< N * N > res{};

   for (auto i = 0U; i < N; ++i)
      for (auto j = 0U; j < N; ++j) res.matrix[i * N + j] = tab[i][j];

   return res;
}

template < size_t N >
constexpr int calculateElems(const Matrix< N >& data, int side) noexcept
{
   auto sum = 0;

   for (auto i = 0; i < side / 2; ++i)
   {
      for (auto j = i + 1; j < side - (i + 1); ++j)
      {
         sum += data.matrix[side * i + j];
      }
   }

   return sum;
}

template < size_t N >
constexpr auto rotateClockwise(const Matrix< N >& data, int side) noexcept
{
   using namespace std;

   Matrix< N > res{};

   for (auto i = 0; i < side; ++i)
   {
      for (auto j = 0; j < side; ++j) res.matrix[i * side + j] = data.matrix[j * side + i];
   }

   return res;
}

template < size_t N >
constexpr auto reverseMatrix(const Matrix< N >& data) noexcept
{
   Matrix< N > result{data};

   for (size_t i = 0; i < N / 2; ++i)
   {
      result.matrix[i]         = data.matrix[N - 1 - i];
      result.matrix[N - 1 - i] = data.matrix[i];
   }

   return result;
}

template < size_t N >
constexpr auto prepareOutputMatrix(const Matrix< N >& data, int side)
{
   auto reversedMatrix = reverseMatrix(data);

   Matrix< 4 > result{};
   result.matrix[0] = calculateElems(data, side);
   result.matrix[1] = calculateElems(rotateClockwise(reversedMatrix, side), side);
   result.matrix[2] = calculateElems(reversedMatrix, side);
   result.matrix[3] = calculateElems(rotateClockwise(data, side), side);

   return result;
}

template < size_t N >
constexpr auto    calculateMatrix(const int (&data)[N][N]) noexcept
{
   using namespace std;

   if (N <= 2) return Matrix< 4 >{};

   return prepareOutputMatrix(makeMatrix(data), N);
};

int main(int argc, char* argv[])
{
   using namespace std;

   int arr[5][5]{
       {0, 1, 2, 3, 4}, {1, 1, 2, 3, 4}, {2, 2, 2, 3, 4}, {3, 3, 3, 3, 4}, {4, 4, 4, 4, 4}};

   int pos = 1;
   for (const auto& i : calculateMatrix(arr))
   {
      std::cout << std::setfill(' ') << std::setw(3) << i;
      if (!(pos % 2)) std::cout << "" << std::endl;
      ++pos;
   }

   return 0;
}
